# Apache Hive on Ubuntu

## Install Dependentcy

****This step support linux Ubuntu only**

Things to prepare before testing

| Tools | Version | Description |
| --- | --- | --- |
| java jdk 8 | [U202 Up](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html) | cmd `sudo apt-get install -y openjdk-8-jdk` |



### Download and install Hive

```sh
./bin/install-hive.sh
```

### Set new variable to apache-hive-hivr-env.sh

```sh

# variable SERVICE
export HADOOP_HOME=/home/hdoop/hadoop-3.3.3


```

#### docker login

```sh
docker login --username ${USER} --password ${PASSWORD}
```

#### Docker Pull Command

```sh

docker pull cloudera/quickstart:latest

```

#### Connect hive command line

```sh
hive --hiveconf fs.defaultFS=hdfs://localhost:8020
```




