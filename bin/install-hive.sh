
STEP="1"

#sudo apt-get update -y
#sudo apt-get install -y zip openjdk-8-jdk

#wget https://dlcdn.apache.org/hadoop/common/hadoop-3.3.3/hadoop-3.3.3.tar.gz
#tar xzf hadoop-3.3.3.tar.gz
#cp -r /home/hdoop/apache-hive-on-ubuntu/hadoop-3.3.3 /home/hdoop

#wget http://archive.apache.org/dist/hive/hive-3.1.3/apache-hive-3.1.3-bin.tar.gz
#tar xzf apache-hive-3.1.3-bin.tar.gz
#cp -r /home/hdoop/apache-hive-on-ubuntu/apache-hive-3.1.3-bin /home/hdoop


## write ~/.bashrc
#echo '# Hadoop configurations' >> ~/.bashrc
#echo 'export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64' >> ~/.bashrc
#echo 'export HADOOP_HOME=/home/hdoop/hadoop-3.3.3' >> ~/.bashrc
#echo 'export PATH=$PATH:$HADOOP_HOME/bin' >> ~/.bashrc
#echo 'export PATH=$PATH:$HADOOP_HOME/sbin' >> ~/.bashrc
#echo 'export HADOOP_MAPRED_HOME=${HADOOP_HOME}' >> ~/.bashrc
#echo 'export HADOOP_COMMON_HOME=${HADOOP_HOME}' >> ~/.bashrc
#echo 'export HADOOP_HDFS_HOME=${HADOOP_HOME}' >> ~/.bashrc
#echo 'export YARN_HOME=${HADOOP_HOME}' >> ~/.bashrc


#echo '# Hive configurations' >> ~/.bashrc
#echo 'export HIVE_HOME=/home/hdoop/apache-hive-3.1.3-bin' >> ~/.bashrc
#echo 'export PATH=$PATH:$HIVE_HOME/sbin:$HIVE_HOME/bin' >> ~/.bashrc
#echo 'export HIVE_CONF_DIR=/home/hdoop/apache-hive-3.1.3-bin/conf' >> ~/.bashrc
#echo 'export CLASSPATH=$CLASSPATH:$HADOOP_HOME/lib/*:$HIVE_HOME/lib/*' >> ~/.bashrc
#echo 'export HADOOP_OPTS="-Djava.library.path=$HADOOP_HOME/lib/native"' >> ~/.bashrc

#source /home/hdoop/.bashrc

#cp /home/hdoop/apache-hive-3.1.3-bin/conf/hive-default.xml.template /home/hdoop/apache-hive-3.1.3-bin/conf/hive-default.xml
#cp /home/hdoop/apache-hive-3.1.3-bin/conf/hive-env.sh.template /home/hdoop/apache-hive-3.1.3-bin/conf/hive-env.sh

#echo 'export HADOOP_HOME=/home/hdoop/hadoop-3.3.3' >> $HIVE_HOME/bin/hive-config.sh

#echo 'export HADOOP_HOME=/home/hdoop/hadoop-3.3.3' >> /home/hdoop/apache-hive-3.1.3-bin/bin/hive-config.sh

## run hadoop create warehouse
/home/hdoop/hadoop-3.3.3/bin/hadoop fs -mkdir -p /user/hive/warehouse
/home/hdoop/hadoop-3.3.3/bin/hadoop fs -chmod 777 /user/hive/warehouse
/home/hdoop/hadoop-3.3.3/bin/hadoop fs -chmod 777 /tmp
#/home/hdoop/apache-hive-3.1.3-bin/bin/schematool -dbType derby -initSchema

